import Vue from 'vue'
import App from './components/App.vue'
import router from './router'

document.addEventListener('DOMContentLoaded', async () => {
  const root = document.getElementById('app-root')
  new Vue(Object.assign({}, App, { router })).$mount(root)
})
