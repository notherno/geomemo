import Vue from 'vue'
import VueRouter from 'vue-router'

import Sample from './components/sample.vue'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [{ path: '/', name: 'index', component: Sample }],
})
