#!/bin/bash

yarn build
pipenv run ./manage.py migrate
pipenv run ./manage.py collectstatic --no-input
