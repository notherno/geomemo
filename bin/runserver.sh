#!/bin/bash

pipenv install -d
pipenv run ./manage.py migrate
exec pipenv run ./manage.py runserver 0.0.0.0:8000
