from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Memo
from .serializers import MemoSerializer


@login_required
def index(request):
    '''Renders application'''
    return render(request, 'index.html', {
        'title': 'Geomemo',
    })


@api_view(['GET'])
def api_memos(request):
    memos = Memo.objects.all()
    return Response(MemoSerializer(memos, many=True).data)
