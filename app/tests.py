from django.test import TestCase
from .models import Memo


class MemoTest(TestCase):
    def test_create(self):
        self.assertEqual(len(Memo.objects.all()), 0)

        memo = Memo(lat=35.697488, lng=139.701354, title='Okubo Park')
        memo.save()

        self.assertEqual(len(Memo.objects.all()), 1)
