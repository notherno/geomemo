from django.db import models


class Memo(models.Model):
    lat = models.FloatField()
    lng = models.FloatField()
    title = models.CharField(max_length=64)
    added_at = models.DateTimeField(auto_now_add=True)
