import webpack = require('webpack')
import path = require('path')
import BundleTracker = require('webpack-bundle-tracker')
import CleanPlugin = require('clean-webpack-plugin')
import VueLoaderPlugin = require('vue-loader/lib/plugin')

enum WebpackMode {
  Production = 'production',
  Development = 'development',
}

interface Argv {
  mode: WebpackMode
  watch?: boolean
  analyze?: boolean
}

export default (env, argv: Argv) => {
  const isDev = argv.mode === WebpackMode.Development
  const bundleDirName = path.resolve(__dirname, './assets/dist')

  const genRules = (): webpack.Rule[] => [
    {
      test: /\.vue$/,
      use: [{ loader: 'vue-loader' }],
    },
    {
      test: /\.tsx?$/,
      use: [
        { loader: 'babel-loader' },
        { loader: 'ts-loader', options: { appendTsSuffixTo: [/\.vue$/] } },
      ],
      exclude: '/node_modules/',
    },
    {
      test: /\.jsx?$/,
      loader: 'babel-loader',
      exclude: file => /node_modules/.test(file) && !/\.vue\.js/.test(file),
    },
    {
      test: /\.css$/,
      oneOf: [
        {
          resourceQuery: /module/,
          use: [
            { loader: 'vue-style-loader' },
            {
              loader: 'css-loader',
              options: {
                modules: true,
                localIdentName: '[local]-[hash:base64:8]',
                minimize: !isDev,
              },
            },
          ],
        },
        {
          use: [
            { loader: 'style-loader' },
            {
              loader: 'css-loader',
              options: {
                modules: false,
                minimize: !isDev,
              },
            },
          ],
        },
      ],
    },
  ]

  const genPlugins = (): webpack.Plugin[] =>
    [
      new CleanPlugin([path.resolve(bundleDirName, './*.*')], {
        root: __dirname,
        dry: false,
        verbose: true,
        watch: !!argv.watch,
        allowExternal: true,
      }),
      new VueLoaderPlugin(),
      new BundleTracker({ filename: './webpack-stats.json' }),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: `"${argv.mode}"`,
          GOOGLE_API_KEY: JSON.stringify(process.env.GOOGLE_API_KEY),
        },
      }),
    ].filter(Boolean)

  return {
    devtool: isDev ? 'inline-source-map' : false,
    entry: {
      main: path.resolve(__dirname, './src/index.ts'),
    },
    output: {
      filename: '[name]-[hash].js',
      path: bundleDirName,
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.vue'],
      alias: {
        vue: 'vue/dist/vue.esm',
      },
    },
    performance: {
      hints: false,
    },
    module: { rules: genRules() },
    optimization: {
      minimize: !isDev,
    },
    plugins: genPlugins(),
  } as webpack.Configuration
}
