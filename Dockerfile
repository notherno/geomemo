FROM python:3.7.0

# Specify charset
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# Upgrade pip and install pipenv
RUN pip install pip --upgrade && pip install pipenv 
