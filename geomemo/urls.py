"""geomemo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
import django.contrib.auth.views as auth_views
from django.urls import path
import app.views as app_views

urlpatterns = [
    path('', app_views.index),
    path('api/memo', app_views.api_memos),
    path('admin/', admin.site.urls),
    path('login', auth_views.login, {
        'template_name': 'admin/login.html',
        'extra_context': {
            'site_header': admin.site.site_header,
            'site_title': admin.site.site_title,
        },
    }),
]
